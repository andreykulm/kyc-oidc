Synopsis
The project represents a OIDC authorization server that currently implements three endpoints:
1) /autorize
2) /token
3) /iserinfo


Motivation
The current version represents a demo that will be improved later to be delivered for different customers.

Installation
1) Run 'mvn clean package' in the root directory
2) Take the generate .war file from the 'target' folder and run it like 'java -jar {filename}.war'
2.1) As a alternative run the project in you IDE.  
3) Be sure to configure the database on your local instance.
