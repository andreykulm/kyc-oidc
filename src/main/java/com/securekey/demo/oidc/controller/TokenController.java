package com.securekey.demo.oidc.controller;

import com.nimbusds.jwt.JWT;
import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.auth.ClientAuthenticationMethod;
import com.nimbusds.oauth2.sdk.auth.ClientSecretBasic;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.ServletUtils;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.openid.connect.sdk.OIDCTokenResponse;
import com.nimbusds.openid.connect.sdk.token.OIDCTokens;
import com.securekey.demo.oidc.model.AuthorizationCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URI;

@Controller
public class TokenController extends OIDCBaseController {

    private static final Logger logger = LoggerFactory.getLogger(TokenController.class);

    @PostConstruct
    private void init() {
        logger.info("init token controller, default access token length {}, default access token lift time {} seconds", ACCESS_TOKEN_LENGTH, ACCESS_TOKEN_LIFETIME);
    }

    @RequestMapping(value = "token", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Object exchangeToken(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        logger.debug("exchange token with request {} {}", httpServletRequest.getMethod(), httpServletRequest.getRequestURI());

        AuthorizationCodeGrant authorizationCodeGrant;
        HTTPRequest httpRequest;
        TokenRequest tokenRequest;

        try {
            httpRequest = ServletUtils.createHTTPRequest(httpServletRequest);
            tokenRequest = TokenRequest.parse(httpRequest);

            if (tokenRequest.getAuthorizationGrant().getType().equals(GrantType.AUTHORIZATION_CODE)) {
                authorizationCodeGrant = (AuthorizationCodeGrant) tokenRequest.getAuthorizationGrant();
            } else {
                return getErrorResponse(OAuth2Error.UNSUPPORTED_GRANT_TYPE);
            }

            // TODO: will use some of these properties later during the project extension
            ClientSecretBasic clientSecretBasic;
            String clientId;
            ClientAuthenticationMethod clientAuthenticationMethod = tokenRequest.getClientAuthentication().getMethod();

            if (clientAuthenticationMethod.equals(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)) {
                //Note that when use CLIENT_SECRET_BASIC, Authorization header value is base64 encoded as clientId:clientSecret
                clientSecretBasic = (ClientSecretBasic) tokenRequest.getClientAuthentication();
                clientId = clientSecretBasic.getClientID().getValue();
            } else {
                return getErrorResponse(OAuth2Error.INVALID_CLIENT, "Unsupported client authentication method " + clientAuthenticationMethod);
            }

            // the code should be coming back from the auth endpoint
            String code = authorizationCodeGrant.getAuthorizationCode().getValue();
            AuthorizationCode authorizationCode = storageService.getAuthCode(code);
            if (authorizationCode == null) {
                return getErrorResponse(OAuth2Error.INVALID_GRANT, "Bad request: authorization code invalid or already been used");
            }

            // check redirect_uri is required in authorization_code grant type
            if (tokenRequest.getAuthorizationGrant().getType().equals(GrantType.AUTHORIZATION_CODE)) {
                URI redirectURI = authorizationCodeGrant.getRedirectionURI();
                // redirect_uri is not null and must match registered client id
                if (redirectURI == null) {
                    return getErrorResponse(OAuth2Error.INVALID_REQUEST, "redirect URI is required in authorization_code grant type");
                }
            }

            // TODO: put jobId
            // construct the response
            AccessToken accessToken = generateAndSaveAccessToken(clientId,
                    authorizationCode.getScope(),
                    authorizationCode.getAcr());

            // sign id token jwt
            JWT idToken = jwtService.signIdToken(clientId, authorizationCode.getScope(), authorizationCode.getNonceStr());
            storageService.removeAuthCode(authorizationCode.getKey());
            logger.info("Id token is " + idToken.getJWTClaimsSet().toJSONObject());

            OIDCTokens tokens = new OIDCTokens(idToken, accessToken, null);
            OIDCTokenResponse oidcAccessTokenResponse = new OIDCTokenResponse(tokens);
            Object jsonResponse = oidcAccessTokenResponse.toJSONObject();
            logger.info("OIDC token response as JSON: " + jsonResponse);
            return jsonResponse;
        } catch (ParseException e) {
            logger.info("Invalid Token request", e);
            return getErrorResponse(OAuth2Error.INVALID_REQUEST, e.getLocalizedMessage());
        } catch (Exception e) {
            logger.error("Server error. ", e);
            return getErrorResponse(OAuth2Error.SERVER_ERROR, e.getLocalizedMessage());
        }
    }

    /**
     * Indicates an error response of the operation.
     * @param error particular error that occurred.
     * @return response entity with the error response.
     */
    private ResponseEntity<Object> getErrorResponse(ErrorObject error) {
        TokenErrorResponse tokenErrorResponse = new TokenErrorResponse(error);
        return new ResponseEntity<>(tokenErrorResponse.toJSONObject(), HttpStatus.valueOf(error.getHTTPStatusCode()));
    }

    /**
     * Indicates an error response of the operation.
     * @param error particular error that occurred.
     * @param description details of the error.
     * @return response entity with the error response.
     */
    private ResponseEntity<Object> getErrorResponse(ErrorObject error, String description) {
        ErrorObject errorObject = new ErrorObject(error.getCode(), description, error.getHTTPStatusCode(), error.getURI());
        TokenErrorResponse tokenErrorResponse = new TokenErrorResponse(errorObject);
        return new ResponseEntity<>(tokenErrorResponse.toJSONObject(), HttpStatus.valueOf(error.getHTTPStatusCode()));
    }
}
