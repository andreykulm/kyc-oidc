package com.securekey.demo.oidc.controller;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWT;
import com.nimbusds.oauth2.sdk.AuthorizationCode;
import com.nimbusds.oauth2.sdk.ResponseType;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.SerializeException;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.openid.connect.sdk.AuthenticationSuccessResponse;
import com.securekey.demo.oidc.model.AuthorizationContext;
import com.securekey.demo.oidc.model.AuthorizationResponse;
import com.securekey.demo.oidc.service.JWTService;
import com.securekey.demo.oidc.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URI;

@Controller
@Slf4j
@RequestMapping(value = "/oidc")
public class OIDCBaseController {

    private final static String ID_TOKEN = "id_token";

    protected final static String SECURITY_CONTEXT = "securityContext";

    @Value("${oidc.accessToken.length}")
    protected int ACCESS_TOKEN_LENGTH;

    @Value("${oidc.accessToken.lifetime}")
    protected long ACCESS_TOKEN_LIFETIME;

    @Autowired
    protected StorageService storageService;

    @Autowired
    protected JWTService jwtService;

    /**
     * Creates an authorization response based on provided authorization context.
     * @param authorizationContext provided authorization context object.
     * @return constructed authorization response.
     * @throws SerializeException
     * @throws JOSEException
     */
    protected AuthorizationResponse createAuthorizationResponse(AuthorizationContext authorizationContext)
            throws SerializeException, JOSEException {

        ResponseType responseType = authorizationContext.getResponseType();
        String clientId = authorizationContext.getClientId();
        String nonceStr = authorizationContext.getNonceStr();

        Scope scope = authorizationContext.getScope();
        URI redirectURI = authorizationContext.getRedirectUri();
        State state = authorizationContext.getState();
        String acrStr = authorizationContext.getAcrStr();
        String ptok = authorizationContext.getPtok();

        AuthorizationCode authCode = null;
        AccessToken accessToken = null;
        JWT idToken = null;

        // process auth code
        if (responseType.contains(ResponseType.Value.CODE)) {
            authCode = new AuthorizationCode();
            storageService.saveAuthCode(clientId, authCode.getValue(), scope.toString(), acrStr);
        }

        // process access token
        if (responseType.contains(ResponseType.Value.TOKEN)) {
            accessToken = generateAndSaveAccessToken(clientId, scope.toString(), acrStr);
        }

        // process id token
        if (responseType.contains(ID_TOKEN)) {
            idToken = jwtService.signIdToken(clientId, scope.toString(), nonceStr);
        }

        // create a response based on the given parameters
        // the code is generated here
        AuthenticationSuccessResponse authenticationSuccessResponse =
                new AuthenticationSuccessResponse(redirectURI, authCode, idToken, accessToken, state, null, null);

        return new AuthorizationResponse(authenticationSuccessResponse.toURI().toString());
    }

    /**
     * Creates and saves an access token.
     * @param clientId id of the client.
     * @param scope authorization context scope.
     * @param acr authorization context acr value.
     * @return created access token.
     */
    protected AccessToken generateAndSaveAccessToken(String clientId, String scope, String acr) {
        // generate access token
        AccessToken accessToken = new BearerAccessToken(ACCESS_TOKEN_LENGTH, ACCESS_TOKEN_LIFETIME, null);
        // store access token
        storageService.saveAccessToken(clientId, accessToken.getValue(), accessToken.getLifetime(), scope, acr);
        return accessToken;
    }
}
