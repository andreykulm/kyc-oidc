package com.securekey.demo.oidc.controller;

import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.ServletUtils;
import com.nimbusds.openid.connect.sdk.AuthenticationRequest;
import com.securekey.demo.oidc.model.AuthorizationContext;
import com.securekey.demo.oidc.model.AuthorizationResponse;
import com.securekey.demo.oidc.model.ClientOrganization;
import com.securekey.demo.oidc.repository.ClientRepository;
import com.securekey.demo.oidc.service.AuthorizationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;


@Slf4j
@Controller
@RequiredArgsConstructor
public class AuthorizationController extends OIDCBaseController {

	private final AuthorizationService authorizationService;
	private final ClientRepository clientRepository;

	@RequestMapping(value = "authorize", method = {RequestMethod.GET, RequestMethod.POST})
	public Object handler(@RequestParam Map<String, String> reqParam,
						  @RequestHeader(name = HttpHeaders.AUTHORIZATION) String authorization,
						  HttpServletRequest httpServletRequest) {
		AuthorizationResponse response = new AuthorizationResponse(""); // blank redirect uri
		try {
			String clientId = reqParam.get("client_id");
			String secretKey = authorization.replace("bearer ", "");
			Optional<ClientOrganization> optionalClientOrganization = clientRepository.findByClientIdAndApiKey(clientId, secretKey);
			if (!optionalClientOrganization.isPresent()) {
				return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			}
			ClientOrganization clientOrganization = optionalClientOrganization.get();
			if (validateScopes(reqParam, clientOrganization) || validateRedirectUri(reqParam, clientOrganization)) {
				return new ResponseEntity<>(HttpStatus.FORBIDDEN);
			}

			HttpSession session = httpServletRequest.getSession(true);
			AuthorizationContext authContext = (AuthorizationContext) session.getAttribute(SECURITY_CONTEXT);
			if (authContext == null) {
				authContext = new AuthorizationContext();
				session.setAttribute(SECURITY_CONTEXT, authContext);
			}
			log.info("Authorization context is {}", authContext);

			response = buildAuthorizationResponse(httpServletRequest, authContext, session);

		} catch (Exception e) {
			log.error("An error occurred during processing of the authorization request " + e.getCause());
		}
		log.info("Redirect uri is: {}", response.getRedirectUri());
		return new ModelAndView("redirect:" + response.getRedirectUri());
	}

	private boolean validateRedirectUri(@RequestParam Map<String, String> reqParam, ClientOrganization clientOrganization) {
		return !clientOrganization.getRedirectURIList().contains(reqParam.get("redirect_uri"));
	}

	private boolean validateScopes(@RequestParam Map<String, String> reqParam, ClientOrganization clientOrganization) {
		return !clientOrganization.getScopesList().containsAll(Arrays.asList(reqParam.get("scope").split(" ")));
	}

	private AuthorizationResponse buildAuthorizationResponse(HttpServletRequest httpServletRequest,
															 AuthorizationContext authContext,
															 HttpSession session) throws Exception {

		HTTPRequest httpRequest = ServletUtils.createHTTPRequest(httpServletRequest);
		AuthenticationRequest authenticationRequest = AuthenticationRequest.parse(httpRequest);
		AuthorizationResponse response = null;

		// Validate and populate the AuthorizationContext object based on the authorization request
		AuthorizationResponse authorizationResponse = authorizationService.populateAuthenticationContext(authenticationRequest, authContext, session);
		if (authorizationResponse == null) {
			response = createAuthorizationResponse(authContext);
		}
		return response;
	}

}
