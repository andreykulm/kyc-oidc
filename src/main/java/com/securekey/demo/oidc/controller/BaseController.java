package com.securekey.demo.oidc.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/oidc")
public class BaseController {

    @RequestMapping("")
    String showHomePage() {
        return "Oidc application works";
    }
}
