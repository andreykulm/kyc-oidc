package com.securekey.demo.oidc.controller;

import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.ServletUtils;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.BearerTokenError;
import com.nimbusds.openid.connect.sdk.UserInfoErrorResponse;
import com.nimbusds.openid.connect.sdk.UserInfoRequest;
import com.nimbusds.openid.connect.sdk.UserInfoResponse;
import com.nimbusds.openid.connect.sdk.UserInfoSuccessResponse;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;
import com.securekey.demo.oidc.model.AccessToken;
import com.securekey.demo.oidc.model.Attribute;
import com.securekey.demo.oidc.model.User;
import com.securekey.demo.oidc.service.AttributeService;
import com.securekey.demo.oidc.service.StorageService;
import com.securekey.demo.oidc.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Controller
public class UserInfoController extends OIDCBaseController {

    private static final Logger logger = LoggerFactory.getLogger(UserInfoController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private AttributeService attributeService;

    @Autowired
    private StorageService storageService;

    @RequestMapping(value = "userinfo", method = {RequestMethod.GET, RequestMethod.POST}, produces = "application/json")
    @ResponseBody
    public Object getUserInfo(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        logger.info("userinfo request {} {}", httpServletRequest.getMethod(), httpServletRequest.getRequestURI());

        HTTPRequest httpRequest;
        UserInfoRequest userInfoRequest;
        try {
            httpRequest = ServletUtils.createHTTPRequest(httpServletRequest);
            userInfoRequest = UserInfoRequest.parse(httpRequest);

            // access token key comes from the request
            String accessTokenKey = userInfoRequest.getAccessToken().getValue();
            AccessToken accessToken = storageService.getAccessToken(accessTokenKey);
            if (accessToken == null) {
                logger.debug("Cannot find given access token {}", accessTokenKey);
                return getErrorResponse(BearerTokenError.INVALID_TOKEN);
            }

            User user = accessToken.getUser();
            List<Attribute> userAttributes = attributeService.findByUserId(user.getId());
            Map<String, String> attributesMap = userService.constructUserAttributesNameValuePair(userAttributes);

            UserInfo userInfo = new UserInfo(new Subject(user.getUsername()));
            Scope scope = Scope.parse(accessToken.getScope()); // show user info as per scope
            // TODO: later the scope validation might be added

            String givenName = attributesMap.get("given_name");
            String familyName = attributesMap.get("family_name");
            userInfo.setName(givenName + " " + familyName);
            userInfo.setEmailVerified(true);

            setUserInfoClaims(attributesMap, userInfo);

            UserInfoResponse userInfoResponse = new UserInfoSuccessResponse(userInfo);
            logger.info("Userinfo response is " + userInfoResponse.toHTTPResponse().getContentAsJSONObject());
            return new ResponseEntity<Object>(userInfoResponse.toHTTPResponse().getContentAsJSONObject(), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("An exception occurred during get user info " + e);
            return getErrorResponse(new BearerTokenError(OAuth2Error.SERVER_ERROR.getCode(), e.getLocalizedMessage()));
        }
    }

    /**
     * Constructs user info object with appropriate claims from user attributes.
     * @param attributesMap a map of the user attributes.
     * @param userInfo user info object to be send as a response.
     */
    private void setUserInfoClaims(Map<String, String> attributesMap, UserInfo userInfo) {
        for (Map.Entry<String, String> entry : attributesMap.entrySet()) {
            userInfo.setClaim(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Indicates an error response from the user info.
     * @param bearerTokenError an error object.
     * @return response entity with appropriate result.
     */
    private ResponseEntity<String> getErrorResponse(BearerTokenError bearerTokenError) {
        UserInfoErrorResponse userInfoErrorResponse = new UserInfoErrorResponse(bearerTokenError);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Www-Authenticate", userInfoErrorResponse.toHTTPResponse().getWWWAuthenticate());
        return new ResponseEntity<>(responseHeaders, HttpStatus.valueOf(bearerTokenError.getHTTPStatusCode()));
    }
}
