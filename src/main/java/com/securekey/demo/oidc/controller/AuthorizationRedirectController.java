package com.securekey.demo.oidc.controller;

import com.securekey.demo.oidc.model.AuthorizationContext;
import com.securekey.demo.oidc.model.AuthorizationResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Slf4j
@Controller
public class AuthorizationRedirectController extends OIDCBaseController {

    @RequestMapping(value = "redirect", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView handler(HttpServletRequest httpServletRequest) throws Exception {
        HttpSession session = httpServletRequest.getSession(true);
        AuthorizationContext authContext = (AuthorizationContext) session.getAttribute(SECURITY_CONTEXT);

        if (authContext == null) {
            ModelAndView mav = new ModelAndView("failure");
            mav.addObject("errorMessage", "Action not allowed. User is not authenticated.");
            return mav;
        }

        AuthorizationResponse response = createAuthorizationResponse(authContext);
        log.info("Redirect uri is: {}", response.getRedirectUri());
        return new ModelAndView("redirect:" + response.getRedirectUri());
    }
}
