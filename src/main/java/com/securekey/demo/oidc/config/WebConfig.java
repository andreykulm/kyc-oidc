package com.securekey.demo.oidc.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Enables mapping of static resources to the particular values.
 */
@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    /**
     * Represents path patterns for the static resources.
     * NOTE: need to map as /oidc in order to handle initial request mapping to this path.
     */
    private static final String[] RESOURCE_PATH_PATTERNS = {"oidc/images/**", "oidc/css/**", "oidc/js/**"};

    /**
     * Represents a classpath locations of the static resources.
     */
    private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
            "classpath:/resources/", "classpath:/static/images/", "classpath:/static/css/", "classpath:/static/js/"
    };

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(RESOURCE_PATH_PATTERNS).addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);
    }

}
