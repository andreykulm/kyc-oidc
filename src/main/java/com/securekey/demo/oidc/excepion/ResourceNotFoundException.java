package com.securekey.demo.oidc.excepion;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    public ResourceNotFoundException(String msg) {
        super(msg);
    }

    public ResourceNotFoundException(String msg, Throwable e) {
        super(msg, e);
    }

    public ResourceNotFoundException(String msg, Object... args) {
        this(String.format(msg, args));
    }

    public ResourceNotFoundException(String msg, Throwable e, Object... args) {
        super(String.format(msg, args), e);
    }

}
