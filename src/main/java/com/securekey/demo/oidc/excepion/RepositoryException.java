package com.securekey.demo.oidc.excepion;

public class RepositoryException extends Exception {

    private static final long serialVersionUID = 1L;

    public RepositoryException(String msg) {
        super(msg);
    }

    public RepositoryException(String msg, Throwable e) {
        super(msg, e);
    }

    public RepositoryException(String msg, Object... args) {
        this(String.format(msg, args));
    }

    public RepositoryException(String msg, Throwable e, Object... args) {
        super(String.format(msg, args), e);
    }
}
