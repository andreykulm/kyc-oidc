package com.securekey.demo.oidc.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Client {

    private final static String URL_SEPARATOR = " ";

    @JsonProperty("client_id")
    private String clientId;

    @JsonProperty("client_secret")
    private String secret;

    @JsonProperty("jwks")
    private String jwks;

    @JsonProperty("jwks_uri")
    private String jwksUri;

    @JsonProperty("client_name")
    private String name;

    @JsonProperty("redirect_uris")
    private List<String> redirectUriList = new ArrayList<>();

    @JsonIgnore
    private String createdTime;

    @JsonIgnore
    private String lastUpdatedTime;

    @JsonIgnore
    private String redirectUris;

    public Client(){}

    public Client(String clientId, String name, List<String> redirectUriList, String createdTime, String lastUpdatedTime) {
        this.clientId = clientId;
        this.name = name;
        setRedirectUriList(redirectUriList);
        this.createdTime = createdTime;
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getJwks() {
        return jwks;
    }

    public void setJwks(String jwks) {
        this.jwks = jwks;
    }

    public String getJwksUri() {
        return jwksUri;
    }

    public void setJwksUri(String jwksUri) {
        this.jwksUri = jwksUri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getRedirectUriList() {
        return redirectUriList;
    }

    public void setRedirectUriList(List<String> redirectUriList) {
        this.redirectUriList = redirectUriList;
        this.redirectUris = (redirectUriList != null ? StringUtils.join(this.redirectUriList, URL_SEPARATOR) : null);
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(String lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public String getRedirectUris() {
        return redirectUris;
    }

    public void setRedirectUris(String redirectUris) {
        this.redirectUris = redirectUris;
        this.redirectUriList.clear();
        if (redirectUris != null && redirectUris.trim().length() > 0) {
            redirectUriList.addAll(Arrays.asList(redirectUris.split(URL_SEPARATOR)));
        }
    }

    @Override
    public String toString() {
        return "Client{" +
                "clientId='" + clientId + '\'' +
                ", secret='" + secret + '\'' +
                ", jwks='" + jwks + '\'' +
                ", jwksUri='" + jwksUri + '\'' +
                ", name='" + name + '\'' +
                ", redirectUriList=" + redirectUriList +
                ", createdTime='" + createdTime + '\'' +
                ", lastUpdatedTime='" + lastUpdatedTime + '\'' +
                ", redirectUris='" + redirectUris + '\'' +
                '}';
    }
}
