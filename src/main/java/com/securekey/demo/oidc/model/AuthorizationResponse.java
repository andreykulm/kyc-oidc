package com.securekey.demo.oidc.model;

import com.securekey.demo.oidc.enums.ErrorType;

public class AuthorizationResponse {

    public enum State { AuthenticationRequired, Error, OK }

    private State state;
    private String redirectUri;
    private ErrorType error;

    public AuthorizationResponse(State state) {
        if (state == State.Error || state == State.OK) {
            throw new IllegalArgumentException("Invalid constructor call.");
        }
        this.state = state;
    }

    public AuthorizationResponse(String redirectUri) {
        this.redirectUri = redirectUri;
        state = State.OK;
    }

    public AuthorizationResponse(ErrorType error) {
        this.error = error;
        state = State.Error;
    }

    public State getState() {
        return state;
    }
    public String getRedirectUri() {
        return redirectUri;
    }
    public ErrorType getError() {
        return error;
    }

}
