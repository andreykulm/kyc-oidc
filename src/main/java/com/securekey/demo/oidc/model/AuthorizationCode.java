package com.securekey.demo.oidc.model;

/**
 * AUTHORIZATION CODE - The OAuth 2.0 authorization code. Required to obtain
 * the ID token and token from the token end-point.
 */
public class AuthorizationCode extends BaseToken {

    /**
     * OPTIONAL. String value used to associate a Client session with an ID
     * Token, and to mitigate replay attacks. The value is passed through
     * unmodified from the Authentication Request to the ID Token. Sufficient
     * entropy MUST be present in the nonce values used to prevent attackers
     * from guessing values. For implementation notes, see Section 15.5.2.
     */
    private String nonceStr;

    /**
     * OPTIONAL. For PKSE support.
     */
    private String codeChallengeStr;

    private String codeChallengeMethodStr;

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getCodeChallengeStr() {
        return codeChallengeStr;
    }

    public void setCodeChallengeStr(String codeChallengeStr) {
        this.codeChallengeStr = codeChallengeStr;
    }

    public String getCodeChallengeMethodStr() {
        return codeChallengeMethodStr;
    }

    public void setCodeChallengeMethodStr(String codeChallengeMethodStr) {
        this.codeChallengeMethodStr = codeChallengeMethodStr;
    }

}
