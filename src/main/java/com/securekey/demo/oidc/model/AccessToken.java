package com.securekey.demo.oidc.model;

/**
 * ACCESS TOKEN - The access token resembles the concept of a physical token
 * or ticket. It permits the bearer access to a specific HTTP resource or
 * web service, which is typically limited by scope and has an expiration
 * time.
 */
public class AccessToken extends BaseToken {

}
