package com.securekey.demo.oidc.model;

import java.util.Date;

public class BaseToken {

    private User user;
    private Client client;
    private String key;
    private String scope;
    private String acr;
    private long createdTime;
    private long expiryTime;

    public BaseToken () {
        createdTime = (new Date()).getTime();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getAcr() {
        return acr;
    }

    public void setAcr(String acr) {
        this.acr = acr;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public long getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(long expiryTime) {
        this.expiryTime = expiryTime;
    }

    @Override
    public String toString() {
        return "BaseToken{" +
                "user=" + user +
                ", client=" + client +
                ", key='" + key + '\'' +
                ", scope='" + scope + '\'' +
                ", acr='" + acr + '\'' +
                ", createdTime=" + createdTime +
                ", expiryTime=" + expiryTime +
                '}';
    }
}
