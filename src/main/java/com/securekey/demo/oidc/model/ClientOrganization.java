package com.securekey.demo.oidc.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Entity
@Data
@Table(name = "client_organization")
public class ClientOrganization {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "client_icon")
	private String clientIcon;

	@Column(name = "home_url")
	private String homeURL;

	@Column(name = "api_key")
	private String apiKey;

	@Column(name = "client_id", unique = true)
	private String clientId;

	@Column(name = "redirect_uri_list")
	private String redirectURIList;

	@Column(name = "scopes_list")
	private String scopesList;

	public List<String> getScopesList(){
		if(scopesList==null){
			return null;
		} else if (scopesList.isEmpty()){
			return Collections.emptyList();
		}else{
			return Arrays.asList(scopesList.split(" "));
		}
	}

	public List<String> getRedirectURIList(){
		if(redirectURIList==null){
			return null;
		} else if (redirectURIList.isEmpty()){
			return Collections.emptyList();
		}else{
			return Arrays.asList(redirectURIList.split(" "));
		}
	}

}
