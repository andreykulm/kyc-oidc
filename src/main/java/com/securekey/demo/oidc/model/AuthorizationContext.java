package com.securekey.demo.oidc.model;

import com.nimbusds.oauth2.sdk.ResponseType;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.openid.connect.sdk.Prompt;

import java.io.Serializable;
import java.net.URI;
import java.util.List;

public class AuthorizationContext implements Serializable {

    private static final long serialVersionUID = 1L;

    public enum AuthenticationMethod { Password, Fido }
    public enum ACR { loa2, loa3 }

    private Scope scope;
    private ResponseType responseType;
    private String clientId;
    private URI redirectUri;
    private State state;
    private String nonceStr;
    private Prompt prompt;
    private String idTokenHintUser;
    private List<String> acrValues;
    private String codeChallengeStr;
    private String codeChallengeMethodStr;

    private AuthenticationMethod authMethod;
    private String acrStr;
    private String ptok;

    public AuthorizationContext() { }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public ResponseType getResponseType() {
        return responseType;
    }

    public void setResponseType(ResponseType responseType) {
        this.responseType = responseType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public URI getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(URI redirectUri) {
        this.redirectUri = redirectUri;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public Prompt getPrompt() {
        return prompt;
    }

    public void setPrompt(Prompt prompt) {
        this.prompt = prompt;
    }

    public String getIdTokenHintUser() {
        return idTokenHintUser;
    }

    public void setIdTokenHintUser(String idTokenHintUser) {
        this.idTokenHintUser = idTokenHintUser;
    }

    public List<String> getAcrValues() {
        return acrValues;
    }

    public void setAcrValues(List<String> acrValues) {
        this.acrValues = acrValues;
    }

    public String getCodeChallengeStr() {
        return codeChallengeStr;
    }

    public void setCodeChallengeStr(String codeChallengeStr) {
        this.codeChallengeStr = codeChallengeStr;
    }

    public String getCodeChallengeMethodStr() {
        return codeChallengeMethodStr;
    }

    public void setCodeChallengeMethodStr(String codeChallengeMethodStr) {
        this.codeChallengeMethodStr = codeChallengeMethodStr;
    }

    public AuthenticationMethod getAuthMethod() {
        return authMethod;
    }

    public void setAuthMethod(AuthenticationMethod authMethod) {
        this.authMethod = authMethod;
    }

    public String getAcrStr() {
        return acrStr;
    }

    public void setAcrStr(String acrStr) {
        this.acrStr = acrStr;
    }

    public boolean isStepupAuthenticationRequired() {
        return !AuthorizationContext.ACR.loa3.toString().equals(acrStr) && acrValues != null && acrValues.contains(AuthorizationContext.ACR.loa3.toString());
    }

    public boolean isConsentRequired() {
        return scope != null && scope.contains("lockbox_creation");
    }

    public String getPtok() {
        return ptok;
    }

    public void setPtok(String ptok) {
        this.ptok = ptok;
    }

    @Override
    public String toString() {
        return "AuthorizationContext{" +
                "scope=" + scope +
                ", responseType=" + responseType +
                ", clientId='" + clientId + '\'' +
                ", redirectUri=" + redirectUri +
                ", state=" + state +
                ", nonceStr='" + nonceStr + '\'' +
                ", prompt=" + prompt +
                ", idTokenHintUser='" + idTokenHintUser + '\'' +
                ", acrValues=" + acrValues +
                ", codeChallengeStr='" + codeChallengeStr + '\'' +
                ", codeChallengeMethodStr='" + codeChallengeMethodStr + '\'' +
                ", authMethod=" + authMethod +
                ", acrStr='" + acrStr + '\'' +
                ", ptok='" + ptok + '\'' +
                '}';
    }
}
