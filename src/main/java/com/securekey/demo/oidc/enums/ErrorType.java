package com.securekey.demo.oidc.enums;

public enum ErrorType {
    PasswordRequired,
    RedirectUriAndClientIdRequired,
    InvalidClientId,
    RedirectUriNotRegistered,
    InvalidRequestObject,
    InvalidRequestObjectResponseType,
    InvalidRequestObjectClientId,
    InvalidRequestObjectRequestURI,
    PermissionTokenIsMissing;
}
