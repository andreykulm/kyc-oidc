package com.securekey.demo.oidc.repository;

import com.securekey.demo.oidc.model.Attribute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("attributeRepository")
public interface AttributeRepository extends JpaRepository<Attribute, Integer> {

    List<Attribute> findByUserId(Integer userId);

    List<Attribute> findAll();
}
