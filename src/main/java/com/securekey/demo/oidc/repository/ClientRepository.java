package com.securekey.demo.oidc.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

import com.securekey.demo.oidc.model.ClientOrganization;

public interface ClientRepository extends JpaRepository<ClientOrganization, Long> {
	Optional<ClientOrganization> findByClientIdAndApiKey(String clientId, String apiKey);
}
