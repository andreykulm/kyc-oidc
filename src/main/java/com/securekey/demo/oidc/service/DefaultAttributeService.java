package com.securekey.demo.oidc.service;

import com.securekey.demo.oidc.model.Attribute;
import com.securekey.demo.oidc.repository.AttributeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("attributeService")
public class DefaultAttributeService implements AttributeService {

    @Autowired
    private AttributeRepository attributeRepository;

    @Override
    public List<Attribute> findByUserId(Integer userId) {
        return attributeRepository.findByUserId(userId);
    }

    @Override
    public List<Attribute> findAll() {
        return attributeRepository.findAll();
    }
}
