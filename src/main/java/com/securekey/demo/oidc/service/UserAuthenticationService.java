package com.securekey.demo.oidc.service;

import com.securekey.demo.oidc.model.AuthorizationContext;
import com.securekey.demo.oidc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Service
public class UserAuthenticationService {

    private final static String SECURITY_CONTEXT = "securityContext";

    private final static String DEFAULT_USER_PASSWORD = "password";

    @Autowired
    private UserService userService;

    // TODO: not used
    public AuthorizationContext authenticate(HttpServletRequest httpServletRequest) {

        HttpSession session = httpServletRequest.getSession(true);
        AuthorizationContext authContext = (AuthorizationContext) session.getAttribute(SECURITY_CONTEXT);
        if (authContext == null) {
            authContext = new AuthorizationContext();
            session.setAttribute(SECURITY_CONTEXT, authContext);
        }

        String username = httpServletRequest.getParameter("username");
        String password = httpServletRequest.getParameter("password");

        User user = userService.findByUsername(username);
        String userPasswordHash = user.getPasswordHash();
        String hashedPassword = userService.hashPassword(password);

//        if ((user.getUsername().equals(username) && hashedPassword.equals(userPasswordHash))
//                || password.equals(DEFAULT_USER_PASSWORD)) {
//            authContext.setAuthenticatedUser(user);
//        } else {
//            authContext.setAuthenticatedUser(null);
//        }
        return authContext;
    }

}
