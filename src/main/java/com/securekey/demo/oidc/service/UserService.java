package com.securekey.demo.oidc.service;

import com.securekey.demo.oidc.model.Attribute;
import com.securekey.demo.oidc.model.User;

import java.util.List;
import java.util.Map;

public interface UserService {

    User findByUsername(String username);

    List<User> findAll();

    Map<String, Attribute> constructUserAttributes(List<Attribute> userAttributes);

    Map<String, String> constructUserAttributesNameValuePair(List<Attribute> userAttributes);

    String hashPassword(String userPassword);

}
