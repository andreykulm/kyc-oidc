package com.securekey.demo.oidc.service;

import com.securekey.demo.oidc.model.Attribute;
import com.securekey.demo.oidc.model.User;
import com.securekey.demo.oidc.repository.UserRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("userService")
public class DefaultUserService implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Map<String, Attribute> constructUserAttributes(List<Attribute> userAttributes) {
        Map<String, Attribute> attributesMap = new HashMap<>();

        for (Attribute attribute: userAttributes) {
            attributesMap.put(attribute.getName(), attribute);
        }
        return  attributesMap;
    }

    @Override
    public Map<String, String> constructUserAttributesNameValuePair(List<Attribute> userAttributes) {
        Map<String, String> attributesMap = new HashMap<>();

        for (Attribute attribute: userAttributes) {
            attributesMap.put(attribute.getName(), attribute.getValue());
        }
        return  attributesMap;
    }

    @Override
    public String hashPassword(String userPassword) {
        return DigestUtils.md5Hex(userPassword);
    }
}
