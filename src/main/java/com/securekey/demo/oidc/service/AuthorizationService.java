package com.securekey.demo.oidc.service;

import com.google.common.base.Strings;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.ResponseType;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.oauth2.sdk.pkce.CodeChallenge;
import com.nimbusds.oauth2.sdk.pkce.CodeChallengeMethod;
import com.nimbusds.openid.connect.sdk.AuthenticationErrorResponse;
import com.nimbusds.openid.connect.sdk.AuthenticationRequest;
import com.nimbusds.openid.connect.sdk.Nonce;
import com.nimbusds.openid.connect.sdk.Prompt;
import com.nimbusds.openid.connect.sdk.claims.ACR;

import com.securekey.demo.oidc.enums.ErrorType;
import com.securekey.demo.oidc.model.AuthorizationContext;
import com.securekey.demo.oidc.model.AuthorizationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class AuthorizationService {

    private static final Logger logger = LoggerFactory.getLogger(AuthorizationService.class);

    private final static String SECURITY_CONTEXT = "securityContext";

    public AuthorizationResponse populateAuthenticationContext(AuthenticationRequest authenticationRequest,
                                                               AuthorizationContext authContext,
                                                               HttpSession session) {

        /*
         * REQUIRED. The OAuth 2.0 response type. For clients using the OAuth code flow
         * it should be set to code. For clients using the implicit flow it should be
         * set to id_token or token id_token.
         */
        ResponseType responseType = authenticationRequest.getResponseType();

        /*
         * REQUIRED. The OAuth 2.0 client identifier, obtained at registration.
         */
        String clientId = authenticationRequest.getClientID().getValue();

        /*
         * REQUIRED. OpenID Connect requests MUST contain the openid scope value.
         * OPTIONAL scope values of profile, email, address, phone, and offline_access
         * are also defined. See Section 2.4 for more about the scope values defined by
         * this document.
         */
        Scope scope = authenticationRequest.getScope();

        /*
         * REQUIRED. The redirection URI to which the response will be sent. It must
         * exactly match one of the registered redirection URIs for the client.
         */
        URI redirectURI = authenticationRequest.getRedirectionURI();

        /*
         * REQUIRED. A permission token provided by the KYC Service
         * that the KYC Service provided in the pre-registration API call.
         * This is a KYC Service specific extension.
         */
        String ptok = authenticationRequest.getCustomParameter("ptok");

        /*
         * RECOMMENDED. Opaque value, e.g. a random string, used to maintain state
         * between the request and the callback. Use of this parameter is not required
         * but highly recommended.
         */
        State state = authenticationRequest.getState();

        /*
         * OPTIONAL. String value used to associate a Client session with an ID Token,
         * and to mitigate replay attacks. The value is passed through unmodified from
         * the Authorization Request to the ID Token. Sufficient entropy MUST be present
         * in the nonce values used to prevent attackers from guessing values.
         */
        Nonce nonce = authenticationRequest.getNonce();
        String nonceStr = nonce == null ? null : nonce.getValue();

        /*
         * OPTIONAL. PKSE support
         */
        CodeChallenge codeChallenge = authenticationRequest.getCodeChallenge();
        String codeChallengeStr = codeChallenge == null ? null : codeChallenge.toString();

        CodeChallengeMethod codeChallengeMethod = authenticationRequest.getCodeChallengeMethod();
        String codeChallengeMethodStr = codeChallengeMethod == null ? null : codeChallengeMethod.toString();

        /*
         * OPTIONAL
         */
        Prompt prompt = authenticationRequest.getPrompt();

        /*
         * OPTIONAL
         */
        List<String> acrValues = null;
        if (authenticationRequest.getACRValues() != null) {
            acrValues = new ArrayList<>();
            for (ACR acr : authenticationRequest.getACRValues()) {
                acrValues.add(acr.getValue());
            }
        }

        /*
         * OPTIONAL - ID Token previously issued by the Authorization Server being passed as a hint about the End-User's
         * current or past authenticated session with the Client. If the End-User identified by the ID Token is logged
         * in or is logged in by the request, then the Authorization Server returns a positive response; otherwise, it
         * SHOULD return an error, such as login_required. When possible, an id_token_hint SHOULD be present when
         * prompt=none is used and an invalid_request error MAY be returned if it is not; however, the server SHOULD
         * respond successfully when possible, even if it is not present. The Authorization Server need not be listed
         * as an audience of the ID Token when it is used as an id_token_hint value.  If the ID Token received by the RP
         * from the OP is encrypted, to use it as an id_token_hint, the Client MUST decrypt the signed ID Token contained
         * within the encrypted ID Token. The Client MAY re-encrypt the signed ID token to the Authentication Server
         * using a key that enables the server to decrypt the ID Token, and use the re-encrypted ID token as the
         * id_token_hint value.
         */
        JWT idTokenHint = authenticationRequest.getIDTokenHint();

        /*
         * Now parse the request object. Its values supersede values passed as query parameters.
         */
        JWT requestObjectJWT = authenticationRequest.getRequestObject();
        if (requestObjectJWT != null) {

            JWTClaimsSet jwtClaimsSet;
            Object objClaim;
            try {
                jwtClaimsSet = requestObjectJWT.getJWTClaimsSet();
            } catch (java.text.ParseException e) {
                return new AuthorizationResponse(ErrorType.InvalidRequestObject);
            }

            objClaim = jwtClaimsSet.getClaim("response_type");

            if (objClaim == null || !responseType.toString().equals(objClaim.toString())) {
                return new AuthorizationResponse(ErrorType.InvalidRequestObjectResponseType);
            }

            objClaim = jwtClaimsSet.getClaim("client_id");
            if (objClaim == null || !clientId.equals(objClaim.toString())) {
                return new AuthorizationResponse(ErrorType.InvalidRequestObjectClientId);
            }

            objClaim = jwtClaimsSet.getClaim("nonce");
            if (objClaim != null) {
                nonceStr = objClaim.toString();
            }

            objClaim = jwtClaimsSet.getClaim("code_challenge");
            if (objClaim != null) {
                codeChallengeStr = objClaim.toString();
            }

            objClaim = jwtClaimsSet.getClaim("code_challenge_method");
            if (objClaim != null) {
                codeChallengeMethodStr = objClaim.toString();
            }

            objClaim = jwtClaimsSet.getClaim("scope");
            if (objClaim != null && !Strings.isNullOrEmpty(objClaim.toString())) {
                scope = new Scope(objClaim.toString().split("\\s+"));
            }

            objClaim = jwtClaimsSet.getClaim("redirect_uri");
            if (objClaim != null) {
                try {
                    redirectURI = new URI(objClaim.toString());
                } catch (URISyntaxException e) {
                    return new AuthorizationResponse(ErrorType.InvalidRequestObjectRequestURI);
                }
            }

            objClaim = jwtClaimsSet.getClaim("state");
            if (objClaim != null && !Strings.isNullOrEmpty(objClaim.toString())) {
                state = new State(objClaim.toString());
            }

            objClaim = jwtClaimsSet.getClaim("prompt");
            if (objClaim != null && !Strings.isNullOrEmpty(objClaim.toString())) {
                prompt = new Prompt(objClaim.toString());
            }

            objClaim = jwtClaimsSet.getClaim("acr_values");
            if (objClaim != null) {
                acrValues = Arrays.asList(objClaim.toString().split(" "));
            }

            objClaim = jwtClaimsSet.getClaim("id_token_hint");

            if (objClaim != null) {
                try {
                    idTokenHint = JWTParser.parse(objClaim.toString());
                } catch (java.text.ParseException e) {
                    return sendErrorResponse(redirectURI, state, OAuth2Error.ACCESS_DENIED, "Invalid id_token_hint.");
                }
            }

            objClaim = jwtClaimsSet.getClaim("ptok");
            if (objClaim != null) {
                ptok = objClaim.toString();
            } else {
                return new AuthorizationResponse(ErrorType.PermissionTokenIsMissing);
            }
        }

        // Determine the expected login user
        String idTokenHintUser = null;
        if (idTokenHint != null) {
            try {
                idTokenHintUser = idTokenHint.getJWTClaimsSet().getSubject();
            } catch (java.text.ParseException e) {
                return sendErrorResponse(redirectURI, state, OAuth2Error.ACCESS_DENIED, "Invalid id_token_hint subject.");
            }
        }

        // populate authentication context

        authContext.setResponseType(responseType);
        authContext.setClientId(clientId);
        authContext.setNonceStr(nonceStr);
        authContext.setCodeChallengeStr(codeChallengeStr);
        authContext.setCodeChallengeMethodStr(codeChallengeMethodStr);
        authContext.setScope(scope);
        authContext.setRedirectUri(redirectURI);
        authContext.setState(state);
        authContext.setPrompt(prompt);
        authContext.setAcrValues(acrValues);
        authContext.setIdTokenHintUser(idTokenHintUser);
        authContext.setPtok(ptok);

        session.setAttribute(SECURITY_CONTEXT, authContext);

        // do not return any of the authorization response if no errors occurred
        return null;
    }

    private AuthorizationResponse sendErrorResponse(URI redirectURI, State state, ErrorObject errorObject, String message) {
        ErrorObject errorObjectNew = new ErrorObject(errorObject.getCode(), message, errorObject.getHTTPStatusCode(), errorObject.getURI());
        AuthenticationErrorResponse authenticationErrorResponse = new AuthenticationErrorResponse(redirectURI, errorObjectNew, state, null);
        String redirect = authenticationErrorResponse.toURI().toString();
        logger.debug("Sent Error redirect: {}", redirect);
        return new AuthorizationResponse(redirect);
    }

}
