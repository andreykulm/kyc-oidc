package com.securekey.demo.oidc.service;

import com.securekey.demo.oidc.model.AccessToken;
import com.securekey.demo.oidc.model.AuthorizationCode;
import com.securekey.demo.oidc.model.Client;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class StorageService {

    private Map<String, AuthorizationCode> codes = new ConcurrentHashMap<>();
    private Map<String, AccessToken> tokens = new ConcurrentHashMap<>();

    public void saveAuthCode(String clientId, String key, String scope, String acr) {
        Client client = new Client();
        client.setClientId(clientId);
        AuthorizationCode authorizationCode = new AuthorizationCode();
        authorizationCode.setClient(client);
        authorizationCode.setKey(key);
        authorizationCode.setScope(scope);
        authorizationCode.setAcr(acr);
        codes.put(authorizationCode.getKey(), authorizationCode);
    }

    public AuthorizationCode getAuthCode(String key) {
        return codes.get(key);
    }

    public void removeAuthCode(String key) {
        codes.remove(key);
    }

    public void saveAccessToken(String clientId, String key, long lifeTime, String scope, String acr) {
        Client client = new Client();
        client.setClientId(clientId);
        AccessToken accessToken = new AccessToken();
        accessToken.setClient(client);
        accessToken.setScope(scope);
        accessToken.setKey(key);
        accessToken.setAcr(acr);
        accessToken.setExpiryTime(accessToken.getCreatedTime() + lifeTime * 1000);
        tokens.put(accessToken.getKey(), accessToken);
    }

    public AccessToken getAccessToken(String key) {
        return tokens.get(key);
    }

    public void removeAccessToken(String key) {
        tokens.remove(key);
    }

}
