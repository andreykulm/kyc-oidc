package com.securekey.demo.oidc.service;

import com.securekey.demo.oidc.model.Attribute;

import java.util.List;

public interface AttributeService {

    List<Attribute> findByUserId(Integer userId);

    List<Attribute> findAll();
}
