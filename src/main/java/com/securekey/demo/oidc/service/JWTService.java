package com.securekey.demo.oidc.service;

import com.google.common.base.Strings;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class JWTService {

    private static final Logger logger = LoggerFactory.getLogger(JWTService.class);

    @Value("${oidc.idToken.issuer}")
    private String issuer;

    @Value("${oidc.idToken.lifetime}")
    private long lifetime;

    @Value("${oidc.idToken.kid}")
    private String kid;

    private static final String NONCE = "nonce";

    public JWT signIdToken(String clientId, String subject, String nonce) throws JOSEException {

        JWSSigner signer = new MACSigner("nRH6MfLQnKbLrINM5K2bhHA8TEauz4t6");

        // Prepare JWT with claims set
        JWTClaimsSet.Builder builder = new JWTClaimsSet.Builder();

        /**
         * iss - REQUIRED. Issuer Identifier for the Issuer of the response. The
         * iss value is a case sensitive URL using the https scheme that
         * contains scheme, host, and optionally, port number and path
         * components and no query or fragment components.
         */
        builder.issuer(issuer);

        /**
         * sub - REQUIRED. Subject Identifier. Locally unique and never
         * reassigned identifier within the Issuer for the End-User, which is
         * intended to be consumed by the Client, e.g., 24400320 or
         * AItOawmwtWwcT0k51BayewNvutrJUqsvl6qs7A4. It MUST NOT exceed 255 ASCII
         * characters in length. The sub value is a case sensitive string.
         */
        builder.subject(subject);

        /**
         * aud - REQUIRED. Audience(s) that this ID Token is intended for. It
         * MUST contain the OAuth 2.0 client_id of the Relying Party as an
         * audience value. It MAY also contain identifiers for other audiences.
         * In the general case, the aud value is an array of case sensitive
         * strings. In the common special case when there is one audience, the
         * aud value MAY be a single case sensitive string.
         */
        builder.audience(clientId);

        /**
         * exp - REQUIRED. Expiration time on or after which the ID Token MUST NOT be accepted for processing.
         */
        Date now = new Date();
        builder.expirationTime(new Date(now.getTime() + lifetime * 1000));

        /**
         * iat - REQUIRED. Time at which the JWT was issued. Its value is a JSON number representing the number of seconds from 1970-01-01T0:0:0Z as measured in UTC until the date/time.
         */
        builder.issueTime(now);

        if(!Strings.isNullOrEmpty(nonce)) {
            builder.claim(NONCE, nonce);
        }

        JWSHeader jwsHeader = new JWSHeader(JWSAlgorithm.HS256, null, null, null, null, null, null, null, null, null, kid, null, null);
        SignedJWT signedJWT = new SignedJWT(jwsHeader, builder.build());

        // Compute the RSA signature
        signedJWT.sign(signer);
        return signedJWT;
    }
}
